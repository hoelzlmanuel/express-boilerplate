import HTTPError, { InternalError } from '../util/errors'

const errorhandler = (err, req, res, next) => {
  if (err) {
    const rply = err => res.status(err.code).json({ code: err.code, error: err.name, detail: err.message })
    if (err instanceof HTTPError) {
      rply(err)
    } else {
      rply(new InternalError())
    }
  } else {
    next()
  }
}

export default errorhandler
