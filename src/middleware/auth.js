import { UnauthorizedError } from '../util/errors'
import models from '../models'

const checkAuth = async (req, res, next) => {
  const key = req.get('x-api-key')
  if (key) {
    const session = await models.session.findOne({
      where: { key },
      attributes: [],
      include: [ {
        model: models.user
      } ]
    })
    if (session) {
      req.user = session.user
      req.sessionKey = key
      next()
    } else {
      next(new UnauthorizedError('Invalid session'))
    }
  } else {
    next(new UnauthorizedError('Missing header'))
  }
}

export default checkAuth
