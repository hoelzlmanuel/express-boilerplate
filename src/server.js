import express from 'express'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import config from 'config'
import path from 'path'
import cors from 'cors'

import { NotFoundError } from './util/errors'
import errorhandler from './middleware/errorhandler'
import router from './router'

const launch = async ({ log, port } = {}) => {
  const app = express()

  if (log) {
    app.use(morgan(config.get('loglevel')))
  }

  app
    .use(cors())
    .use(bodyParser.json())
    .use('/api', router)
    .all('*', (req, res, next) => {
      next(new NotFoundError())
    })
    .use(errorhandler)

  return new Promise(resolve => {
    const server = app.listen(port || config.get('server.port'), '0.0.0.0', () => {
      console.log(`Server running on ${server.address().port}`)
      resolve(server)
    })
  })
}

export default launch
