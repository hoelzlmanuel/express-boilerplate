import status from './status'
import { login, logout, getuser } from './auth'

const runRoute = route => async (req, res, next) => {
  try {
    const ret = await route(req, res)
    if (ret !== undefined) {
      res.status(200).json(ret)
    } else {
      res.status(204).end()
    }
  } catch (e) {
    next(e)
  }
}

export default runRoute
export {
  status,
  login,
  logout,
  getuser
}
