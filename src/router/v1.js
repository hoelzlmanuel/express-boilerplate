import express from 'express'
import runRoute, { login, logout, status, getuser } from '../routes'
import checkAuth from '../middleware/auth'

const { Router } = express

const router = Router()

router
  .get('/status', runRoute(status))

  .post('/login', runRoute(login))
  .post('/logout', checkAuth, runRoute(logout))
  .get('/user', checkAuth, runRoute(getuser))

export default router
