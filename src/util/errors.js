export default class HTTPError extends Error {
  constructor(code, name, ...params) {
    super(...params)
    this._code = code
    this._name = name
  }
  
  get code() {
    return this._code
  }
  
  get name() {
    return this._name
  }
}
  
export class BadRequestError extends HTTPError {
  constructor(...params) {
    super(400, 'Bad Request', ...params)
  }
  
}
  
export class UnauthorizedError extends HTTPError {
  constructor(...params) {
    super(401, 'Unathorized', ...params)
  }
}
  
export class NotFoundError extends HTTPError {
  constructor(...params) {
    super(404, 'Not found', ...params)
  }
}
  
export class InternalError extends HTTPError {
  constructor(...params) {
    super(500, 'Internal Server Error', ...params)
  }
}
  
