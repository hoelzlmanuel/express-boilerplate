import models from '../models'
import bcrypt from 'bcrypt'
import nanoid from 'nanoid'
import { BadRequestError, UnauthorizedError } from './errors'

const getUser = async username => {
  return models.user.findOne({
    where: {
      username
    },
    raw: true
  })
}

export const logIn = async ({ username, password }) => {
  const user = await getUser(username)
  if (!user) throw new BadRequestError('User doesn\'t exist')
  if (await bcrypt.compare(password, user.password)) {
    const key = nanoid()
    const session = (await models.session.create({
      userId: user.id, 
      key
    }))
    return { key: session.key }
  } else {
    throw new UnauthorizedError('Invalid password')
  }
}

export const logOut = async ({ key }) => {
  await models.session.destroy({
    where: {
      key
    }
  })
}

export const createUser = async ({ username, password }) => {
  if (await getUser(username)) throw new BadRequestError('Username exists already')
  const hash = await bcrypt.hash(password, 10)
  const user = await models.user.create({ username, password: hash })
  delete user.password
  return user
}
