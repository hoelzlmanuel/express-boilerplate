'use strict'

import fs from 'fs'
import path from 'path'
import Sequelize from 'sequelize'
import Umzug from 'umzug'
import sequelize_fixtures from 'sequelize-fixtures'

const basename = path.basename(__filename)
const env = process.env.NODE_ENV || 'development'
const config = { ...require(__dirname + '/../../config/local-database.json')[env], logging: process.env.LOG_LEVEL !== 'none' ? console.log : false }
const db = {}

let sequelize
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config)
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config)
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
  })
  .forEach(file => {
    const model = sequelize['import'](path.join(__dirname, file))
    db[model.name] = model
  })

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

export default db

const umzug = new Umzug({
  storage: 'sequelize',

  storageOptions: {
    sequelize: sequelize
  },

  migrations: {
    params: [
      sequelize.getQueryInterface(), Sequelize
    ],
    path: path.join(__dirname, '..', '..', 'db', 'migrations')
  }
})

export const migrate = ({ direction } = { direction: 'up' }) => {
  if (direction === 'up') {
    return umzug.up()
  } else if (direction === 'down') {
    return umzug.down({ to: 0 })
  }
}

export const mock = async () => {
  await migrate({ direction: 'down' })
  await migrate()
  const files = [ 'user', 'session' ]
  for (let file of files) {
    await sequelize_fixtures.loadFile(path.resolve(__dirname, '..', '..', 'db', 'fixtures', `${file}.json`), db, { log: process.env.LOG_LEVEL !== 'none' ? console.log : () => {} })
  }
}
