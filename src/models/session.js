'use strict'
module.exports = (sequelize, DataTypes) => {
  const session = sequelize.define('session', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    userid: {
      type: DataTypes.INTEGER
    },
    key: {
      type: DataTypes.CHAR
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    tableName: 'session'
  })
  session.associate = function(models) {
    session.belongsTo(models.user)
  }
  return session
}
