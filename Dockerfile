FROM node:10

# install sqlite
RUN apt-get update && apt-get install sqlite rename -y

# change workdir
WORKDIR /app

# install packages
COPY package*.json ./
RUN npm ci

# copy files
COPY src/ src/
COPY config/ config/
COPY db/ db/
COPY gulpfile.babel.js .
COPY .babelrc .

# build
RUN npm run build:prod

# cleanup
RUN rm -rf src

# rename config
WORKDIR /app/config
RUN rename 's/docker/local/' *

# entrypoint
WORKDIR /app
ENTRYPOINT ["npm", "run", "start"]
