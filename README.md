# EXPRESS BOILERPLATE
simple express boilerplate using babel to transpile es6 and sequelize

## Setup
1. `git clone git@github.com:alex-zach/express-boilerplate.git`
2. delete .git folder (`rm -rf .git`)
3. init new git repo (`git init`)
2. rename package in package.json
3. create local-database.json and docker-database.json

## Run development server
```
npm run start:dev
```

## Test
```
npm test
```

## Run in docker container
```
docker build . -t <projectname>
docker run -p 8080:8080 <projectname>:latest
```