import '@babel/polyfill'
import launch from '../src/server'
import rp from 'request-promise'
import { mock } from '../src/models'
import fs from 'fs'
import path from 'path'

const dirname = path.resolve(process.cwd(), 'test', 'routes')
const excluded = [ 'util.js' ]

const tests = []
fs
  .readdirSync(dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file.slice(-3) === '.js') && (!excluded.includes(file))
  })
  .forEach(file => {
    const module = require(path.resolve(dirname, file))
    for (let k in module) {
      tests.push(module[k])
    }
  })


describe('Test routes', () => {
  let server
  let baseUrl

  before(async () => {
    server = await launch({ log: false })
    const { address, port } = server.address()
    baseUrl = `http://${address}:${port}`
  })

  after(() => {
    if (server) server.close()
  })

  tests.forEach(test => {
    describe(`${test.method.toUpperCase()} ${test.path}`, () => {
      before(async () => { 
        if (test.mock) {
          await mock()
        }
      })

      test.tests.forEach(subtest => {
        it(subtest.name, async () => {
          let err = null, response = null
          try {
            response = await rp(Object.assign({
              url: `${baseUrl}${test.path}`,
              method: test.method,
              resolveWithFullResponse: true,
              json: true
            }, subtest.requestoptions))
          } catch (e) {
            err = e
          }
          await subtest.test(err, response)
        })
      })
    })
  })
})
